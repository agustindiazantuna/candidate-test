----------------------------------------------------------------------------------
-- Archivo:       counter.vhd
-- Descripcion:   Contador de modulo M.
-- Entradas:         clk,rst        clock y reset
-- Salidas:          tc             terminal count
--
-- Autor:         Agustin Diaz Antuna
-- Contacto:      agustin.diazantuna@gmail.com
-- Fecha:         14-11-2015
----------------------------------------------------------------------------------


----------------------------------------------------------------------------------
-- library
----------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;


----------------------------------------------------------------------------------
-- entity
----------------------------------------------------------------------------------
entity counter is
   generic ( M : integer := 4 );
   port (   CLK      : in  std_logic;
            RST      : in  std_logic;
            EN       : in  std_logic;
            TC       : out std_logic;
            COUNT    : out std_logic_vector(integer(ceil(log2(real (M))))-1 downto 0)
         );
end counter;

architecture behavioral of counter is

   -- constants
   constant N : integer := integer(ceil(log2(real (M))));

   -- signals
   signal count_a : std_logic_vector (N-1 downto 0) := (others => '0');
   signal count_f : std_logic_vector (N-1 downto 0) := (others => '0');

begin

   -- memory
   process(CLK)
   begin
      if rising_edge(CLK) then
         if RST = '1' then
            count_a <= (others => '0');
         elsif EN = '1' then
            count_a <= count_f;
         end if;
      end if;
   end process;


   process(RST, count_a, EN)
   begin
      if RST = '1' or to_integer(unsigned(count_a)) = (M-1) then
         count_f <= (others => '0');
      elsif EN = '1' then
         count_f <= std_logic_vector(unsigned(count_a) + 1);
      else
         count_f <= count_a;
      end if;
   end process;


   TC <=    '1'   when RST = '0' and EN = '1' and to_integer(unsigned(count_a)) = (M-1)
      else  '0';


   -- output
   COUNT <= count_a;


end behavioral;



