----------------------------------------------------------------------------------
-- Archivo:       spi.vhdd
-- Descripcion:   SPI core
--
-- Ejercicio:     ej1
--
-- Autor:         Agustin Diaz Antuna
-- Contacto:      agustin.diazantuna@gmail.com
-- Fecha:         06-11-2018
----------------------------------------------------------------------------------

----------------------------------------------------------------------------------
-- library
----------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;


----------------------------------------------------------------------------------
-- entity
----------------------------------------------------------------------------------
entity spi is
    generic (
        C_AXI_DATA_WIDTH : integer := 32;
        C_AXI_ADDR_WIDTH : integer := 4;
        SPI_DATA_WIDHT   : integer := 8
    );
    port (
        AXI_CLK      : in std_logic;
        AXI_RESETn   : in std_logic;
        -- Control ports
        INT          : out std_logic;
        WDATA        : in std_logic_vector(C_AXI_DATA_WIDTH-1 downto 0);
        RDATA        : out std_logic_vector(C_AXI_DATA_WIDTH-1 downto 0);
        WADDR        : in std_logic_vector(C_AXI_ADDR_WIDTH-1 downto 0);
        RADDR        : in std_logic_vector(C_AXI_ADDR_WIDTH-1 downto 0);
        WENA         : in std_logic;
        RENA         : in std_logic;
        -- SPI ports
        CSn          : out std_logic;
        MOSI         : out std_logic;
        MISO         : in std_logic;
        SCK          : out std_logic
    );
end spi;

architecture arch_spi of spi is

   -- constants
   signal R_TX    : integer := 0;
   signal R_RX    : integer := 4;
   signal ZEROS   : std_logic_vector(C_AXI_DATA_WIDTH-SPI_DATA_WIDHT-1 downto 0) := (others => '0');

   -- signals
   -- RDATA register next
   signal RDATA_N : std_logic_vector(SPI_DATA_WIDHT-1 downto 0);

   -- enable SPI while count < C_AXI_DATA_WIDTH
   signal ENA_R : std_logic;
   signal ENA_N : std_logic;

   -- DATA COUNTER
   signal DATA_COUNTER_RST : std_logic;
   signal COUNT : std_logic_vector(integer(ceil(log2(real (SPI_DATA_WIDHT+1))))-1 downto 0);

   -- CLK DIV to generate SCLK
   signal CLK_DIV_RST : std_logic;
   signal TC : std_logic;
   signal HALF_TC : std_logic;

   -- MOSI register
   signal MOSI_R : std_logic_vector(SPI_DATA_WIDHT-1 downto 0);
   signal MOSI_N : std_logic_vector(SPI_DATA_WIDHT-1 downto 0);

   -- MISO register
   signal MISO_R : std_logic_vector(SPI_DATA_WIDHT-1 downto 0);
   signal MISO_N : std_logic_vector(SPI_DATA_WIDHT-1 downto 0);


begin

   -- RDATA register
   process(AXI_CLK)
   begin
      if rising_edge(AXI_CLK) then
         if AXI_RESETn = '0' then
            RDATA <= (others => '0');
         else
            RDATA <= ZEROS & RDATA_N;
         end if;
      end if;
   end process;

   -- RDATA next
   RDATA_N <=  MISO_R   when  to_integer(unsigned(RADDR)) = R_RX and RENA = '1'
      else     MOSI_R   when  to_integer(unsigned(WADDR)) = R_TX and RENA = '1'
      else     (others => '0');


  -- ENA register when WENA = '1'
   process(AXI_CLK)
   begin
      if rising_edge(AXI_CLK) then
         if AXI_RESETn = '0' then
            ENA_R <= '0';
         else
            ENA_R <= ENA_N;
         end if;
      end if;
   end process;

   -- ENA next
   ENA_N <= '0'      when  (to_integer(unsigned(COUNT)) = 8 or AXI_RESETn = '0')
      else  '1'      when  (WENA = '1' and to_integer(unsigned(WADDR)) = R_TX and ENA_R = '0')
      else ENA_R;


   -- data counter 0 to SPI_DATA_WIDHT+1
   DATA_COUNTER: entity work.counter
      generic map (
         M => SPI_DATA_WIDHT+1)
      port map (
         CLK   => AXI_CLK,
         RST   => DATA_COUNTER_RST,
         EN    => TC,
         TC    => open,
         COUNT => COUNT
         );

   DATA_COUNTER_RST <=  '1'   when (AXI_RESETn = '0') or (WENA = '1' and to_integer(unsigned(WADDR)) = R_TX) or (to_integer(unsigned(COUNT)) = 8)
   else                 '0';


   -- clk divider to generate SCK
   CLK_DIVIDER: entity work.clk_div
      generic map (
         M => 100 )
      port map (
         CLK      => AXI_CLK,
         RST      => CLK_DIV_RST,
         EN       => ENA_R,
         TC       => TC,
         HALF_TC  => HALF_TC,
         CLK_O    => SCK
         );

   CLK_DIV_RST <= '1'   when  (AXI_RESETn = '0') or (WENA = '1' and to_integer(unsigned(WADDR)) = R_TX)
      else        '0';


   -- MOSI register
   process(AXI_CLK)
   begin
      if rising_edge(AXI_CLK) then
         if AXI_RESETn = '0' then
            MOSI_R <= (others => '0');
         elsif (WENA = '1' and to_integer(unsigned(WADDR)) = R_TX) then
            MOSI_R <= WDATA(SPI_DATA_WIDHT-1 downto 0);
         else
            MOSI_R <= MOSI_N;
         end if;
      end if;
   end process;

   -- shift register
   MOSI_N <=   MOSI_R(SPI_DATA_WIDHT-2 downto 0) & '0'    when TC = '1'
      else     MOSI_R;

   -- MOSI output
   MOSI <= MOSI_R(SPI_DATA_WIDHT-1);


   -- MISO register
   process(AXI_CLK)
   begin
      if rising_edge(AXI_CLK) then
         if AXI_RESETn = '0' or WENA = '1' then
            MISO_R <= (others => '0');
         else
            MISO_R <= MISO_N;
         end if;
      end if;
   end process;

   -- shift register
   MISO_N <=   MISO_R(SPI_DATA_WIDHT-2 downto 0) & MISO   when  (ENA_R = '1' and HALF_TC = '1' and TC = '0')
      else     MISO_R;


   -- outputs
   INT <=   '1'   when  to_integer(unsigned(COUNT)) = SPI_DATA_WIDHT and ENA_R = '1'
      else  '0';
   CSn <= not(ENA_R);


end architecture arch_spi;



