----------------------------------------------------------------------------------
-- Archivo:       clk_div.vhd
-- Descripcion:   Contador de modulo M.
-- Entradas:         clk,rst        clock y reset
-- Salidas:          tc             terminal count
--
-- Autor:         Agustin Diaz Antuna
-- Contacto:      agustin.diazantuna@gmail.com
-- Fecha:         14-11-2015
----------------------------------------------------------------------------------


----------------------------------------------------------------------------------
-- library
----------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;


----------------------------------------------------------------------------------
-- entity
----------------------------------------------------------------------------------
entity clk_div is
   generic ( M : integer := 10 );
   port (   CLK      : in  std_logic;
            RST      : in  std_logic;
            EN       : in  std_logic;
            TC       : out std_logic;
            HALF_TC  : out std_logic;
            CLK_O    : out std_logic
         );
end clk_div;

architecture behavioral of clk_div is

   -- constants
   constant N : integer := integer(ceil(log2(real (M))));

   -- signals
   signal sig_tc        : std_logic;
   signal sig_half_tc   : std_logic;
   signal count         : std_logic_vector (N-1 downto 0) := (others => '0');
   signal count_next    : std_logic_vector (N-1 downto 0) := (others => '0');
   signal clk_o_next    : std_logic;

begin


   process(CLK)
   begin
      if rising_edge(CLK) then
         if RST = '1' then
            count <= (others => '0');
         elsif EN = '1' then
            count <= count_next;
         end if;
      end if;
   end process;


   process(RST, count)
   begin
      sig_half_tc <= '0';
      sig_tc <= '0';

      if RST = '1' then
         count_next <= (others => '0');
      elsif to_integer(unsigned(count)) = (M-1) then
         count_next <= (others => '0');
         sig_half_tc <= '1';
         sig_tc <= '1';
      else
         count_next <= std_logic_vector(unsigned(count) + 1);
         if to_integer(unsigned(count)) = (M/2-1) then
            sig_half_tc <= '1';
         end if;
      end if;
   end process;


   -- fft
   process(clk)
   begin
      if rising_edge(clk) then
         if RST = '1' then
            clk_o_next <= '0';
         elsif (EN = '1' and sig_half_tc = '1') then
            clk_o_next <= not(clk_o_next);
         end if;
      end if;
   end process;


   -- output
   CLK_O <= clk_o_next;
   tc <= sig_tc;
   HALF_TC <= sig_half_tc;


end behavioral;



